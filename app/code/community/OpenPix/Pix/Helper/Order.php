<?php

class OpenPix_Pix_Helper_Order extends Mage_Core_Helper_Abstract {

    use OpenPix_Pix_Trait_ExceptionMessenger;
    use OpenPix_Pix_Trait_LogMessenger;

    public function getStoreName() {
        $store = Mage::app()->getStore();
        return $store->getName();
    }

    public function handlePayloadCharge($orderId){
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

        if($order && $order->getId()) {
            $correlationID = $this->helper()->uuid_v4();

            $grandTotal = $order->getGrandTotal();

            $payload = [
                'correlationID' => $correlationID,
                'value' => $this->helper()->get_amount_openpix($grandTotal),
                'comment' => $this->getStoreName(),
            ];

            return $payload;
        }

        return [];
    }

    public function handleCreateCharge($data) {
        $app_ID = $this->helper()->getAppID();

        if (!$app_ID) {
            $this->log("OpenPix - AppID not found");
            $this->error("OpenPix - AppID not found");
            return false;
        }

        $apiUrl = $this->helper()->getOpenPixApiUrl();

        $headers = [
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: " . $app_ID,
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $apiUrl . '/api/openpix/v1/charge');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));

        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if (curl_errno($curl) || $response === false) {
            $this->log("OpenPix - Error creating Pix");
            $this->error("OpenPix - Error creating Pix");
            curl_close($curl);

            return false;
        }

        curl_close($curl);

        if ($statusCode === 401) {
            $this->log("OpenPix - Invalid AppID");
            $this->error("OpenPix - Invalid AppID");

            return false;
        }

        if ($statusCode !== 200) {
            $this->log("OpenPix - Error creating Pix");
            $this->error("OpenPix - Error creating Pix");
            curl_close($curl);

            return false;
        }

        return json_decode($response, true);
    }

    public function handleResponseCharge($responseBody, $orderId) {
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

        $order->setOpenpixCorrelationid($responseBody["charge"]["correlationID"]);
        $order->setOpenpixPaymentlinkurl($responseBody["charge"]["paymentLinkUrl"]);
        $order->setOpenpixQrcodeimage($responseBody["charge"]["qrCodeImage"]);
        $order->setOpenpixBrcode($responseBody["charge"]["brCode"]);

        $order->save();

        $additional = [
            'openpix_correlationid' => $responseBody["charge"]["correlationID"],
            'openpix_paymentlinkurl' => $responseBody["charge"]["paymentLinkUrl"],
            'openpix_qrcodeimage' => $responseBody["charge"]["qrCodeImage"],
            'openpix_brcode' => $responseBody["charge"]["brCode"],
        ];

        $order->getPayment()->setAdditionalInformation($additional)->save();

        return ['success' => true, 'additional' => $additional];
    }

    public function addInformation($order, $additional){
        if($order && $order->getId() && is_array($additional) && count($additional) >= 1){
            foreach ($additional as $key => $value) {
                $order->getPayment()->setAdditionalInformation($key, $value)->save();
            }
        }
    }

    protected function helper(){
        return Mage::helper('openpix_pix');
    }
}