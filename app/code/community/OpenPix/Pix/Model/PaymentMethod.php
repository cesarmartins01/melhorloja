<?php

class OpenPix_Pix_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract
{
    use OpenPix_Pix_Trait_ExceptionMessenger;
    use OpenPix_Pix_Trait_LogMessenger;

    protected $_canOrder            = true;
    protected $_allowCurrencyCode   = ['BRL'];

    protected $_code = 'openpix_pix';
    protected $_formBlockType       = 'openpix_pix/form';
    protected $_infoBlockType       = 'openpix_pix/info';

    public function getInformation() {
        return $this->getConfigData('information');
    }

    // generate order and qr code pix on openpix
    public function order(Varien_Object $payment, $amount)
    {
        $this->log("OpenPix - Order Start ");
        try {
            if($this->canOrder()){
                $order = $this->getInfoInstance()->getOrder();
                $orderIncrementId = $order->getIncrementId();

                try {
                    $payload = $this->helper()->handlePayloadCharge($orderIncrementId);
                } catch (Exception $e) {
                    $this->log("OpenPix - handlePayloadCharge Error" . $e->getMessage());
                    $this->error($e->getMessage());
                    return false;
                }

                try {
                    $responseBody = $this->helper()->handleCreateCharge($payload);
                } catch (Exception $e) {
                    $this->log("OpenPix - handleCreateCharge Error" . $e->getMessage());
                    $this->error($e->getMessage());
                    return false;
                }

                try {
                    $responseCharge = $this->helper()->handleResponseCharge($responseBody, $orderIncrementId);
                    $this->log("OpenPix - responseCharge " . json_encode($responseCharge));
                } catch (Exception $e) {
                    $this->log("OpenPix - handleResponseCharge Error" . $e->getMessage());
                    $this->error($e->getMessage());
                    return false;
                }

                if($responseCharge['success']){
                    $this->helper()->addInformation($order, $responseCharge['additional']);
                }
            }
        } catch (Exception $e) {
            $this->log("OpenPix - Payment Method: Error" . $e->getMessage());
            $this->error($e->getMessage());
        }
    }

    protected function helper(){
        return Mage::helper('openpix_pix/order');
    }
}